FROM debian:stretch-slim
RUN apt-get update && apt-get install -y build-essential devscripts debhelper haskell-platform haskell-stack dh-make atool
